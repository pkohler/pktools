
$updateguid="0a7a0d55-de2a-4446-90f7-2fa2f36ecc47";
$Session=New-Object -ComObject "Microsoft.Update.Session";
$Searcher = $Session.CreateUpdateSearcher();

# search update history for that guid
Write-Host "searching history..."
$historyCount = $Searcher.GetTotalHistoryCount()
$res = $Searcher.QueryHistory(0, $historyCount)# | where-object { $_.ServiceID -match $updateguid }
Write-Host "$($res.Count) updates found in history."
foreach ($u in $res) {
    $u.ServiceID
}

# search new updates for that guid
Write-Host "searching online updates...."
$sres = $Searcher.Search("UpdateID='$updateguid'");
$upds = $sres.Updates;
$num = $upds.Count;
Write-Host "found $num updates";
if ($num -gt 0) {
    foreach ($u in $upds) {$u} 
} else {
    Write-Host "No new updates found matching $updateguid"
};
