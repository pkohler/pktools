$Session = New-Object -ComObject "Microsoft.Update.Session"
$Searcher = $Session.CreateUpdateSearcher()
$historyCount = $Searcher.GetTotalHistoryCount()
Write-Host "Found $historyCount past patch actions"
Write-Host "Searching for new updates (this could take a while)"
# valid search strings are per https://msdn.microsoft.com/en-us/library/aa386526%28v=vs.85%29.aspx
# eg. to search for an update by GUID, use "UpdateID=0000-0000-000000-0000-0000"
$sres = $Searcher.Search("IsInstalled=0")

# check the results of our search
$upds = $sres.Updates
$num = $upds.Count
Write-Host "found $num updates"

if ($num > 0) {
    # download the updates
    $dldr = $Session.CreateUpdateDownloader()
    $dldr.Updates = $upds
    Write-Host "downloading $num updates..."
    $r = $dldr.Download()
    Write-Host "download result: $r"

    # install the updates
    $instlr = $Session.CreateUpdateInstaller()
    $instlr.Updates = $upds
    $r = $instlr.Install()
    Write-Host "install result: $($r.ResultCode)"
    Write-Host "reboot required: $($r.RebootRequired)"
}