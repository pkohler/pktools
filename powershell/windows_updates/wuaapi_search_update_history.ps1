# Check in a remote system's update history to see if an update was installed.
# This should work well from an Onsite Manager to check status on another system

# To check on the local system, just comment out the line containing Invoke-Command
# and the very last }
$targetSystem = 'remotehostname_or_ipaddress'

if ($targetSystem -eq "remotehostname_or_ipaddress") {
    $targetSystem = "."  # use local
}

# If you want to see the whole history, set this to '.'
# Otherwise, it will search as a regex, which means you can just put in 'KB3101746' and it'll search
# for that, or you can use regex's flexibility to search for eg. 'KB(3101746|3101722)' to find 
# either KB3101746 or KB3101722
# For more info or regex, I recommend MDN's article: 
# https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions
$searchRegex = 'KB(3101746|3101722)'


Invoke-Command -ComputerName $targetSystem -ScriptBlock {
    # create a windows update session
    $Session = New-Object -ComObject "Microsoft.Update.Session"
    $Searcher = $Session.CreateUpdateSearcher()
    $historyCount = $Searcher.GetTotalHistoryCount()
    # get the entire history (from entry 0 to entry $historyCount)
    $Searcher.QueryHistory(0, $historyCount) | Select-Object Title, Description, Date, @{
        name = "Operation"
        expression = {
            switch($_.operation) {
                1 {"Installation"}
                2 {"Uninstallation"}
                3 {"Other"}
            }
        }
    } | where-object {
        # filter out any entries that don't match $searchregex
        $_.Title -match $searchRegex
    }
}

<#
  output will look something like this:

  Title               Description         Date                Operation          
  -----               -----------         ----                ---------          
  Security Update ... This security up... 15/12/2015 02:55... Installation       
#>

# one-liner version, to run on the device directly through `powershell -Command "pasteHere"`:
# $searchregex='KB(3134700|3126446|3134228|3126587|3139914)';$Session = New-Object -ComObject 'Microsoft.Update.Session';$Searcher = $Session.CreateUpdateSearcher();$historyCount = $Searcher.GetTotalHistoryCount();$Searcher.QueryHistory(0, $historyCount) | Select-Object Title, Description, Date, @{name = 'Operation';expression = {switch($_.operation) {1 {'Installation'}; 2 {'Uninstallation'}; 3 {'Other'}}}} | where-object {$_.Title -match $searchRegex};