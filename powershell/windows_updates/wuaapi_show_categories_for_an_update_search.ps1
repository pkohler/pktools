# Search for updates and show their categories according to Microsoft.
# This can help you ensure you're syncing/approving the correct patch categories in WSUS or
# third-party update management solutions.
# all arguments are combined with "AND"; args are ignored if left blank.
# https://msdn.microsoft.com/en-us/library/aa386526%28v=vs.85%29.aspx for details about IUpdateSearcher

param (
    [Parameter(
        Mandatory=$false,
        ValueFromPipeline=$True,
        ValueFromPipelineByPropertyName=$True,
        HelpMessage='search for patches by their deployment action. Should be one of "Installation" or "Uninstallation"'
    )][string]$deploymentAction,
    [Parameter(
        Mandatory=$false,
        ValueFromPipeline=$True,
        ValueFromPipelineByPropertyName=$True,
        HelpMessage='search for patches by their type, eg. "Driver" or "Software"'
    )][string]$type,
    [Parameter(
        Mandatory=$false,
        ValueFromPipeline=$True,
        ValueFromPipelineByPropertyName=$True,
        HelpMessage='find updates that are intended for deployment by Automatic Updates'
    )][int]$isAssigned,
    [Parameter(
        Mandatory=$false,
        ValueFromPipeline=$True,
        ValueFromPipelineByPropertyName=$True,
        HelpMessage='include optional updates'
    )][int]$browseOnly,
    [Parameter(
        Mandatory=$false,
        ValueFromPipeline=$True,
        ValueFromPipelineByPropertyName=$True,
        HelpMessage='find updates that are flagged to be automatically selected by Windows Update'
    )][int]$autoSelectOnWebSites,
    [Parameter(
        Mandatory=$false,
        ValueFromPipeline=$True,
        ValueFromPipelineByPropertyName=$True,
        HelpMessage='search for a patch by its UpdateID as defined by Microsoft, eg. "15561620-b712-4a0e-8de8-f0c27b3a007a"'
    )][string]$uid,
    [Parameter(
        Mandatory=$false,
        ValueFromPipeline=$True,
        ValueFromPipelineByPropertyName=$True,
        HelpMessage='find updates that belong to the specified category (by category uid)'
    )][string]$category,
    [Parameter(
        Mandatory=$false,
        ValueFromPipeline=$True,
        ValueFromPipelineByPropertyName=$True,
        HelpMessage='find updates that are already installed'
    )][int]$isInstalled,
    [Parameter(
        Mandatory=$false,
        ValueFromPipeline=$True,
        ValueFromPipelineByPropertyName=$True,
        HelpMessage='Finds updates that are marked as hidden (ie. potentially superseded) on the computer.'
    )][int]$isHidden,
    [Parameter(
        Mandatory=$false,
        ValueFromPipeline=$True,
        ValueFromPipelineByPropertyName=$True,
        HelpMessage='find updates that are present (installed for one or more products) on the computer'
    )][int]$isPresent,
    [Parameter(
        Mandatory=$false,
        ValueFromPipeline=$True,
        ValueFromPipelineByPropertyName=$True,
        HelpMessage='find updates that require a reboot'
    )][int]$rebootRequired
)

$ErrorActionPreference = "Stop"
$Session = New-Object -ComObject "Microsoft.Update.Session"
$Searcher = $Session.CreateUpdateSearcher()

$flagArgArr = @(
    @("RebootRequired", $rebootRequired),
    @("IsPresent", $isPresent),
    @("IsHidden", $isHidden),
    @("IsInstalled", $isInstalled),
    @("AutoSelectOnWebSites", $autoSelectOnWebSites),
    @("BrowseOnly", $browseOnly),
    @("IsAssigned", $isAssigned),
    @("DeploymentAction", $deploymentAction),
    @("Type", $type),
    @("UpdateID", $uid),
    @("Categories", $category)
)

# filter out any null or empty args
function isValidInt([int]$num, [string]$paramName) {
    # make sure the int is 0 or 1
    if ($num -gt 1) {
        # invalid entry
        Write-Error "Invalid entry $num for $paramName - should be -1 (ignore), 0 (false), or 1 (true)"
        $false
    } elseif ($num -lt 0) {
        # valid entry that should get skipped
        $false
    } else {
        # valid entry that should be used
        $true
    }
}
function isValidArg($argEntry) {
    (isValidInt $f[1] $f[0]) -and (![string]::IsNullOrWhiteSpace($f[1]))
}
$filteredArgs = @()
foreach ($f in $argArr) {
    if (isValidArg $f) {
        $filteredArgs += $f
    }
}

function searchWUA() {
    # combine args into a valid search string
    $searchStr = ""
    $i = 1
    foreach ($f in $filteredArgs) {
        if ($f[1] -eq $true -or $f[1] -eq $false) {
            # dealing with a switch; convert to int
            $searchStr += "$($f[0])=$(validateInt $f[1])"
        } else {
            # dealing with a string value, enclose it in single quotes
            $searchStr += "$($f[0])='$($f[1])'"
        }
        if ($i -lt $filteredArgs.Count) {
            $searchStr += " AND "
        }
        $i ++
    }
    # Searching will typically take some time. WUA isn't very fast. This is especially true if you use 
    # broad search terms. 
    Write-Host "Searching Windows Updates for '$searchArgs' - this could take a while..."
    $sres = $Searcher.Search("$searchArgs")
    Write-Host "Update search completed."
    $sres.Updates
}

function printCategories($updates) {
    foreach ($update in $updates) {
        # print title and categories for every update in the search results.
        $cats = ConvertTo-Json $update.Categories -Compress
        Write-Host "$($update.Title) - categories: $cats"
    }
}

printCategories (searchWUA)