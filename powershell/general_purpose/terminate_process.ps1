param(
    [string]$regex1,
    [string]$regex2,
    [string]$regex3,
    [switch]$terminate=$true # this is a boolean for testing what procs would be killed
)


function find_and_kill {
    # match one or more process(es) by the supplied regex and terminate them
    # this will terminate *ALL* processes that match the expression, so choose your expression carefully
    param (
        [Parameter(
            Mandatory=$True,
            ValueFromPipeline=$True,
            ValueFromPipelineByPropertyName=$True,
            HelpMessage='A regular expression for finding the correct process to terminate.'
         )][string]$cmdLineRegex
    )
    $proc = Get-WmiObject Win32_Process | where-object { $_.CommandLine -match $cmdLineRegex }
    if ($proc) {
        $count = $proc.Count
        Write-Host "$count process(es) detected matching ""$cmdlineRegex"":"
        foreach ($p in $proc) {
            Write-Host '    ' $p.CommandLine '[ PID:' $p.ProcessID ']
'        }
        if ($terminate) {
            Write-Host "terminating now..."
            foreach ($p in $proc) {
                $ppid = $p.ProcessId
                $exe = $p.Name
                $ret = $p.Terminate().ReturnValue
                Write-Host "termination of $exe with PID $ppid returned code $ret"
                if ($ret -ne 0) {
                    # bail out!
                    Start-Sleep -Seconds 2
                    System.Exit($ret)
                }
            }
        }
        
    } else {
        Write-Host "No processes found matching ""$cmdlineRegex"""
    }
    # sleep so MW automation has a chance to capture the ouput
    Start-Sleep -Seconds 2 
}

if (![string]::IsNullOrEmpty($regex1)) {
    find_and_kill($regex1)
}
if (![string]::IsNullOrEmpty($regex2)) {
    find_and_kill($regex2)
}
if (![string]::IsNullOrEmpty($regex3)) {
    find_and_kill($regex3)
}