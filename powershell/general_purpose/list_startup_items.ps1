[uint32]$hklm = 2147483650  # 0x80000002
[uint32]$hkcu = 2147483649  # 0x80000001
$reg = New-Object WMIClass "root\default:StdRegProv";

function enumRegStrings([uint32]$hive, [string]$key) {
    if ($hive -eq $hklm) {
        $strHive = 'HKLM';       
    } elseif ($hive -eq $hkcu) {
        $strHive = 'HKCU';
    }
    Write-Host "$strHive\$key";
    Write-Host "";
    $arrNames = $reg.EnumValues($hive, $key).sNames;
    foreach ($n in $arrNames) {
        $v = $reg.GetStringValue($hive, $key, $n);
        Write-Host "[$n] $($v.sValue)";
    }
    Write-Host "";
    Write-Host "";
}

$win32 = 'SOFTWARE\Microsoft\Windows\CurrentVersion\Run';
$win64 = 'SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Run';

enumRegStrings $hklm $win32;
enumRegStrings $hklm $win64;
enumRegStrings $hkcu $win32;
enumRegStrings $hkcu $win64;