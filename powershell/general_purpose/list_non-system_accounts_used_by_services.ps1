param (
    # run this script with -verbose to log all services, even if they're using standard service accounts
    [switch]$verbose
)

function ProcessService($service) { 
    switch ($service.StartName) { 
        # "LocalSystem", "NT AUTHORITY\LocalService" and "NT AUTHORITY\NetworkService" are standard
        # local accounts; ignore them unless running with -verbose
        "LocalSystem" {
            if ($verbose) {
                Write-Host ("{0}: {1}" -f $service.Name, $service.StartName) 
            }
        }
        "NT AUTHORITY\LocalService" {
            if ($verbose) {
                Write-Host ("{0}: {1}" -f $service.Name, $service.StartName) 
            }
        }
        "NT AUTHORITY\NetworkService" {
            if ($verbose) {
                Write-Host ("{0}: {1}" -f $service.Name, $service.StartName)
            }
        }
        default {
            Write-Host ("{0}: {1}" -f $service.Name, $service.StartName)
        }
    }
}

Write-Host "Beginning check of services on this system. Verbose logging is $verbose"
$Services = Get-WmiObject -class Win32_Service
foreach ($s In $Services) {
        ProcessService $s
}
Write-Host "Completed evaluating accounts used by services on this system"