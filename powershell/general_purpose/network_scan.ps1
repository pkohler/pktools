$ErrorActionPreference = "Stop";
# This is the slowest, most naive way to scan a network and find open TCP ports.
# I recommend avoiding using it if at all possible. Use NMap or similar instead - see nmap.org
# That said, if all you've got is powershell and you aren't allowed to install anything, it'll do
# the job.
$subnet = "10.10.10."

function IsTCPListening([string]$ipaddr, [int]$port) {
    try {
        $sock= New-Object System.Net.Sockets.TCpClient($ipaddr, $port);
        $open = $sock.Connected;
        $sock.Close();
    } catch {
        $open = $false
    }
    $open
}

function TCPPortScan(
    [string] $ipaddr,
    [int]    $start=1,
    [int]    $end=1024) {
    # this is going to be painfully slow; but it'll work in the absence of better tools
    Write-Host "Checking for open TCP ports on $ipaddr. Scanning $($end-$start+1) ports starting at $start"
    for ($i=$start; $i -lt $end; $i++) {
        $open = IsTCPListening $ipaddr $i
        if ($open) {
            Write-Host "port $i is open on $ipaddr"
        }
    }
}

$ping = New-Object System.Net.NetworkInformation.Ping

for ($i=0;$i -lt 255;$i++) {
    $ipaddr = "$subnet.$i"
    if ($ping.Send($ipaddr).Status -eq "Success") {
        tcpportscan $ipaddr
    }
}
