[uint32]$hklm = 2147483650;
[uint32]$hku = 2147483651;
$reg = [WMIClass]"root\default:StdRegProv";
$plist = 'SOFTWARE\Microsoft\Windows NT\CurrentVersion\ProfileList\';
foreach ($sid in $reg.EnumKey($hku, "").sNames) {
    $pip = $reg.GetStringValue($hklm, $plist + $sid, 'ProfileImagePath').sValue
    if (![string]::IsNullOrWhiteSpace($pip)) {
        Write-Host $pip
    }
}