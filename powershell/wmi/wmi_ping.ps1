$ErrorActionPreference = "Stop"

$target = "."
$max = 1000
$interval = 15
$outfile = "test.txt"

$count = 0
$logger = [System.IO.File]::AppendText($outfile)

while ($count -ne $max) {
    $out = [DateTime]::Now.ToString()
    $out += "- WMI Time Query Results: "
    try {
        $wmi = Get-WmiObject -Class Win32_OperatingSystem -ComputerName $target -Property LocalDateTime;
        $out += $wmi.LocalDateTime
    } catch {
        $out += $_.Exception.ToString()
    }
    Write-Host $out
    $logger.WriteLine($out)
    $logger.Flush()
    $count++
    Start-Sleep -Seconds $interval
}
$logger.Close()