function makeDir ([string]$dirPath) {
    #create a dir if it doesn't exist
    if (!(Test-Path $dirPath -PathType Container)) {
        New-Item -ItemType directory -Path $dirPath
    }
}

makeDir "C:\temp"
$f = New-Object System.IO.StreamWriter("C:\temp\WMIClassesAndProperties.txt")
# you can set the computername property to the dns/hostname or ip address of a remote system
# assuming the current user has WMI access to it.
foreach ($w in Get-WmiObject -ComputerName "." -List * | Sort-Object Name) {
    foreach ($p in $w.Properties) {
        Write-Host "$($w.Name) $($p.Name)"
        $f.WriteLine("$($w.Name) $($p.Name)")
    }
} 
$f.Close() 


# oneliner for WMI classes:
if (!(Test-Path 'c:\temp' -PathType Container)){New-Item -ItemType directory -Path $dirPath}$f = New-Object System.IO.StreamWriter('C:\temp\WMIClasses.txt');foreach ($w in Get-WmiObject -ComputerName "." -List * | Sort-Object Name) {Write-Host $w.Name;$f.WriteLine($w.Name);}$f.Close();