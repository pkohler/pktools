param (
    [string]$wmiclass1 = "",
    [string]$wmiclass2 = "",
    [string]$wmiclass3 = ""
)

$ErrorActionPreference = "Stop"

$failures = @()


function CheckWMIClass {
    param($classname)
    try {
        $instances = Get-WmiObject -Class $classname -Namespace 'root\cimv2';
        if ($instances -ne $null) {
            $count = $instances.Count
            if ($count -ge 0) {
                $isValid = "okay ($count instances detected)"
            } else {
                $isValid = "okay (singleton)"
            }
        } else {
            $isValid = "okay (instanceless singleton)"
        }
        Write-Host  $classname": " $isValid
    } catch {
        $global:failures += $classname
        Write-Host $classname": failed - WMI class does not exist on this system"
    }
}


#check any classes supplied as args:
foreach ($c in ($wmiclass1,$wmiclass2,$wmiclass3)) {
    if ($c -ne "") {
        CheckWMIClass($c)
    }
}


# check common perf counter classes that should always be present
foreach ($c in (
        'Win32_PerfFormattedData_PerfDisk_LogicalDisk',
        'Win32_PerfFormattedData_PerfDisk_PhysicalDisk',
        'Win32_PerfFormattedData_PerfNet_Browser',
        'Win32_PerfFormattedData_PerfNet_Redirector',
        'Win32_PerfFormattedData_PerfNet_Server',
        'Win32_PerfFormattedData_PerfOS_Cache',
        'Win32_PerfFormattedData_PerfOS_Memory',
        'Win32_PerfFormattedData_PerfOS_Objects',
        'Win32_PerfFormattedData_PerfOS_PagingFile',
        'Win32_PerfFormattedData_PerfOS_Processor',
        'Win32_PerfFormattedData_PerfOS_System',
        'Win32_PerfFormattedData_PerfProc_JobObject',
        'Win32_PerfFormattedData_PerfProc_JobObjectDetails',
        'Win32_PerfFormattedData_PerfProc_Process',
        'Win32_PerfFormattedData_PerfProc_Thread',
        'Win32_PerfFormattedData_RemoteAccess_RASPort',
        'Win32_PerfFormattedData_RemoteAccess_RASTotal',
        'Win32_PerfFormattedData_Spooler_PrintQueue',
        'Win32_PerfFormattedData_Tcpip_ICMP',
        'Win32_PerfFormattedData_Tcpip_ICMPv6',
        'Win32_PerfFormattedData_Tcpip_IPv4',
        'Win32_PerfFormattedData_Tcpip_IPv6',
        'Win32_PerfFormattedData_Tcpip_NetworkInterface',
        'Win32_PerfFormattedData_Tcpip_TCPv4',
        'Win32_PerfFormattedData_Tcpip_TCPv6',
        'Win32_PerfFormattedData_Tcpip_UDPv4',
        'Win32_PerfFormattedData_Tcpip_UDPv6'
        )
    ) {
    CheckWMIClass($c)
}


If ($failures.Count -ne 0) {
    [Console]::Error.WriteLine("The following WMI classes do not exist:")
    foreach ($f in $failures) {
        [Console]::Error.WriteLine($f)
    }
    Start-Sleep -Seconds 3
    [Environment]::Exit(4204)
} Else {
    Start-Sleep -seconds 3
    [Environment]::Exit(0)
}
