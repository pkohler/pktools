function logText([string]$text, [string]$logpath) {
    # append $text to $logpath
    $writer = [System.IO.File]::AppendText($logpath)
    $writer.WriteLine($text)
    $writer.Close()
}