Add-Type -AssemblyName System.Management;
Import-Module Microsoft.WSMan.Management;

function convertWmiDate($datetimestr) {
    [System.Management.ManagementDateTimeConverter]::ToDateTime($datetimestr);
}

function wsmanQuery([string]$wmiQuery, [string]$target){
    Get-WSManInstance -Enumerate wmicimv2/* -Filter $wmiQuery -ComputerName $target 
}