param (
    [string]$ipaddr = '127.0.0.1',
    [int]$startPort = 1,
    [int]$endPort = 49151,
    [int]$timeoutInMS = 6000
)

$ErrorActionPreference = 'Stop';

# This script will check for open TCP ports within the defined range on the target host, then checks
# each open port for a banner and for various services to attempt to determine what's listening.
# 
# It's going to be painfully slow for big scan ranges, but that's what you get when you try to
# recreate nmap in powershell.

$enc = [System.Text.Encoding]::GetEncoding('ASCII')

# this is a set of base-64 encoded test messages for various TCP services, taken from nmap-service-probes
# responses from these probes can indicate what service is listening on an open port.
$msgs = @{
    'afp'='AAMAAQAAAAAAAAACAAAAAA8A';
    'ajp'='EjQAAQo=';
    'apple-iphoto'='R0VUIC9zZXJ2ZXItaW5mbyBIVFRQLzEuMQ0KQ2xpZW50LURQQVAtVmVyc2lvbjogMVwuMQ0KVXNlci1BZ2VudDogaVBob3RvLzkuMS4xICAoTWFjaW50b3NoOyBOOyBQUEMpDQoNCg==';
    'Arucer'='wuXl5Z6g16Sm0NXd3MjW3dfVyNHWg4DI3aTRocik0tXX3aOkod2m192Y5Q==';
    'beast2'='NjY2';
    'couchbase-data'='gBAAAAAAAAAAAAAAFfDRYgAAAAAAAAAA';
    'DistCCD'='RElTVDAwMDAwMDAxQVJHQzAwMDAwMDA1QVJHVjAwMDAwMDAyY2NBUkdWMDAwMDAwMDItY0FSR1YwMDAwMDAwNm5tYXAuY0FSR1YwMDAwMDAwMi1vQVJHVjAwMDAwMDA2bm1hcC5vRE9USTAwMDAwMDAw';
    'DNSStatusRequest'='AAwAABAAAAAAAAAAAAA=';
    'DNSVersionBindReq'='AB4ABgEAAAEAAAAAAAAHdmVyc2lvbgRiaW5kAAAQAAM=';
    'docker'='R0VUIC92ZXJzaW9uIEhUVFAvMS4xDQoNCg==';
    'dominoconsole'='XCNTVAo=';
    'drda'='ADLQAQABACwQQQAEEV4ABBFtAAQRWgAYFAQUAwAHJAcACCQPAAgUQAAIFHQACAAEEUc=';
    'epmd'='AAFu';
    'erlang-node'='AAtuAAAAAAEEbm1AcA==';
    'firebird'='AAAAAQAAABMAAAACAAAAJAAAAAtzZXJ2aWNlX21ncgAAAAACAAAAEwEIc2Nhbm5lciAEBW5tYXAgBgAAAAAACAAAAAEAAAACAAAAAwAAAAIAAAAKAAAAAQAAAAIAAAADAAAABA==';
    'FourOhFourRequest'='R0VUIC9uaWNlJTIwcG9ydHMlMkMvVHJpJTZFaXR5LnR4dCUyZWJhayBIVFRQLzEuMA0KDQo=';
    'GenericLines'='DQoNCg==';
    'GetRequest'='R0VUIC8gSFRUUC8xLjANCg0K';
    'giop'='R0lPUAEAAQAkAAAAAAAAAAEAAAABAAAABgAAAGFiY2RlZgAABAAAAGdldAAAAAAA';
    'gkrellm'='Z2tyZWxsbSAwLjAuMA==';
    'hazelcast-http'='R0VUIC9oYXplbGNhc3QvcmVzdC9jbHVzdGVyIEhUVFAvMS4wDQoNCg0K';
    'Hello'='RUhMTw0K';
    'Help'='SEVMUA0K';
    'HELP4STOMP'='SEVMUAoKAA==';
    'hp-pjl'='GyUtMTIzNDVYQFBKTCBJTkZPIElEDQobJS0xMjM0NVgNCg==';
    'HTTPOptions'='T1BUSU9OUyAvIEhUVFAvMS4wDQoNCg==';
    'ibm-db2'='AcIAAAAEAAC2AQAAU1FMREIyUkEAAQAABAEBAAUAHQCIAAAAAQAAgAAAAAEJAAAAAQAAQAAAAAEJAAAAAQAAQAAAAAEIAAAABAAAQAAAAAEEAAAAAQAAQAAAAEAEAAAABAAAQAAAAAEEAAAABAAAQAAAAAEEAAAABAAAQAAAAAEEAAAAAgAAQAAAAAEEAAAABAAAQAAAAAEAAAAAAQAAQAAAAAAEAAAABAAAgAAAAAEEAAAABAAAgAAAAAEEAAAAAwAAgAAAAAEEAAAABAAAgAAAAAEIAAAAAQAAQAAAAAEEAAAABAAAQAAAAAEQAAAAAQAAgAAAAAEQAAAAAQAAgAAAAAEEAAAABAAAQAAAAAEJAAAAAQAAQAAAAAEJAAAAAQAAgAAAAAEEAAAAAwAAgAAAAAEAAAAAAAAAAAAAAAABBAAAAQAAgAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAQAAAAAEAAAAAAQAAQAAAAAAgICAgICAgIAAAAAAAAAAAAAAAAAAAAAABAP8AAAAAAAAAAAAAAAAAAAAAAAAAAADkBAAAAAAAAAAAAAAAAAAAAAAAAAB/';
    'ibm-db2-das'='AAAAAERCMkRBUyAgICAgIAEEAAAAEDl6AAEAAAAAAAAAAAAAAQwAAAAAAAAMAAAADAAAAAQ=';
    'ibm-mqseries'='VFNIIAAAAOwBATEAAAAAAAAAAAAAAAERBLgAAElEICAKJgAAAAAAAAAAf/YGQAAAAAAAAFNZU1RFTVwuQURNSU5cLlNWUkNPTk5RAAS4bm1hcC1wcm9iZSAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgAAAAAQBqAAAA/wD///////////////////8AAAAAAAAAAAAKAAAAAAAAAAAAAAAAAAAAAk1RSkIwMDAwMDAwMENBTk5FRF9EQVRBICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIA==';
    'informix'='AJQBPAAAAGQAZQAAAD0ABklFRUVNAABsc3FsZXhlYwAAAAAAAAY5LjI4MAAADFJEU1wjUjAwMDAwMAAABXNxbGkAAAABMwAAAAAAAAAAAAEABW5tYXAAAAVubWFwAG9sAAAAAAAAAAAAPXRsaXRjcAAAAAAAAQBoAAsAAAADAAVubWFwAAAAAAAAAAAAAAAAagAAAH8=';
    'JavaRMI'='SlJNSQACSw==';
    'Kerberos'='AAAAcWqBbjCBa6EDAgEFogMCAQqkgV4wXKAHAwUAUIAAEKIEGwJOTaMXMBWgAwIBAKEOMAwbBmtyYnRndBsCTk2lERgPMTk3MDAxMDEwMDAwMDBapwYCBB8eudmoFzAVAgESAgERAgEQAgEXAgEBAgEDAgEC';
    'kumo-server'='lADN79FhkQM=';
    'LANDesk-RC'='VE5NUAQAAABUTk1FAAAEAA==';
    'LDAPBindReq'='MAwCAQFgBwIBAgQAgAA=';
    'LPDString'='AWRlZmF1bHQK';
    'Memcache'='c3RhdHMNCg==';
    'memcached'='c3RhdHMNCg=='
    'metasploit-msgrpc'='R0VUIC9hcGkgSFRUUC8xLjANCg0K';
    'metasploit-xmlrpc'='PD94bWwgdmVyc2lvbj0iMS4wIiA/PjxtZXRob2RDYWxsPjxtZXRob2ROYW1lPm5tYXAucHJvYmU8L21ldGhvZE5hbWU+PC9tZXRob2RDYWxsPgoA';
    'minecraft-ping'='/gE=';
    'mongodb'='QQAAADowAAD/////1AcAAAAAAAB0ZXN0LiRjbWQAAAAAAP////8bAAAAAXNlcnZlclN0YXR1cwAAAAAAAADwPwA=';
    'ms-sql-s'='EgEANAAAAAAAABUABgEAGwABAgAcAAwDACgABP8IAAFVAAAATVNTUUxTZXJ2ZXIASA8AAA==';
    'mydoom'='DQ0=';
    'NCP'='RG1kVAAAABcAAAABAAAAABERAP8B/xM=';
    'NessusTPv10'='PCBOVFAvMS4wID4K';
    'NessusTPv11'='PCBOVFAvMS4xID4K';
    'NessusTPv12'='PCBOVFAvMS4yID4K';
    'NJE'='1tfF1UBAQEDGwdLFQEBAQAAAAADGwdLFQEBAQAAAAAAA';
    'NotesRPC'='OgAAAC8AAAACAABAAg8AAQA9BQAAAAAAAAAAAAAAAC8AAAAAAAAAAABAHwAAAAAAAAAAAAAAAAAAAAAA';
    'NULL'='';
    'OfficeScan'='R0VUIC8/Q0FWSVQgSFRUUC8xLjENCg0K';
    'OpenVPN'='AA44N6UmCKIboLEAAAAAAA==';
    'oracle-tns'='AFoAAAEAAAABNgEsAAAIAH//fwgAAAABACAAOgAAAAAAAAAAAAAAAAAAAATmAAAAAQAAAAAAAAAAKENPTk5FQ1RfREFUQT0oQ09NTUFORD12ZXJzaW9uKSk=';
    'pervasive-btrieve'='PABLAAAAIAAAAAAAAAAAAP////8AAAoEoL5TA1VSAAA8AAAABQAAAAAAAAAAABoAPAAAAAAACgAAAAAA';
    'pervasive-relational'='Q2xpZW50IHN0cmluZyBmb3IgUEFSQyB2ZXJzaW9uIDEgV2lyZSBFbmNyeXB0aW9uIHZlcnNpb24gMQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA';
    'Radmin'='AQAAAAEAAAAICA==';
    'redis-server'='XCoxDQpcJDQNCmluZm8NCg==';
    'riak-pbc'='AAAAAQc=';
    'RPCCheck'='gAAAKHL+HRMAAAAAAAAAAgABhqAAAZd8AAAAAAAAAAAAAAAAAAAAAAAAAAA=';
    'RTSPRequest'='T1BUSU9OUyAvIFJUU1AvMS4wDQoNCg==';
    'SIPOptions'='T1BUSU9OUyBzaXA6bm0gU0lQLzIuMA0KVmlhOiBTSVAvMi4wL1RDUCBubTticmFuY2g9Zm9vDQpGcm9tOiA8c2lwOm5tQG5tPjt0YWc9cm9vdA0KVG86IDxzaXA6bm0yQG5tMj4NCkNhbGwtSUQ6IDUwMDAwDQpDU2VxOiA0MiBPUFRJT05TDQpNYXgtRm9yd2FyZHM6IDcwDQpDb250ZW50LUxlbmd0aDogMA0KQ29udGFjdDogPHNpcDpubUBubT4NCkFjY2VwdDogYXBwbGljYXRpb24vc2RwDQoNCg==';
    'SMBProgNeg'='AAAApP9TTUJyAAAAAAgBQAAAAAAAAAAAAAAAAAAAQAYAAAEAAIEAAlBDIE5FVFdPUksgUFJPR1JBTSAxLjAAAk1JQ1JPU09GVCBORVRXT1JLUyAxLjAzAAJNSUNST1NPRlQgTkVUV09SS1MgMy4wAAJMQU5NQU4xLjAAAkxNMS4yWDAwMgACU2FtYmEAAk5UIExBTk1BTiAxLjAAAk5UIExNIDAuMTIA';
    'Socks4'='BAEAFn8AAAFyb290AA==';
    'Socks5'='BQQAAQKABQEAAwpnb29nbGUuY29tAFBHRVQgLyBIVFRQLzEuMA0KDQo=';
    'SqueezeCenter_CLI'='c2VydmVyc3RhdHVzDQo=';
    'SSLSessionReq'='FgMAAFMBAABPAwA/R9f3uizu6rJgfvMA/YJ7udWWyHeb5sTbPD3bb+8QbgAAKAAWABMACgBmAAUABABlAGQAYwBiAGEAYAAVABIACQAUABEACAAGAAMBAA==';
    'SSLv23SessionReq'='gJ4BAwEAdQAAACAAAGYAAGUAAGQAAGMAAGIAADoAADkAADgAADUAADQAADMAADIAAC8AABsAABoAABkAABgAABcAABYAABUAABQAABMAABIAABEAAAoAAAkAAAgAAAYAAAUAAAQAAAMHAMAGAEAEAIADAIACAIABAIAAAAIAAAHkaTwr9tabu9OBn78VwUClbxQsTSDEx+C2sLIf+SnomA==';
    'tarantool'='c2hvdyBpbmZvDQo=';
    'teamspeak-tcpquery-ver'='dmVyDQo=';
    'TerminalServer'='AwAACwbgAAAAAAA=';
    'TLS-PSK'='FgMAAHUBAABxAwNVOCpiRVRYU0pEU1pOSE1ERkFPTkRLSlhYWllaSFdIUgAAMACKAIsAjACNAI4AjwCQAJEAkgCTAJQAlQCoAKkAqgCrAKwArQCuAK8AsgCzALYAtwEAABgADQAUABIAAQACAAMBAQECAQMCAQICAgM=';
    'TLSSessionReq'='FgMAAGkBAABlAwNVHKfkcmFuZG9tMXJhbmRvbTJyYW5kb20zcmFuZG9tNAAADAAvAAoAEwA5AAQA/wEAADAADQAsACoAAQADAAIGAQYDBgICAQIDAgIDAQMDAwIEAQQDBAIBAQEDAQIFAQUDBQI=';
    'tn3270'='//sY//oYAElCTS0zMjc5LTQtRf/w';
    'tor-versions'='AAAHAAgAAwAEAAUABg==';
    'Verifier'='U3Vic2NyaWJlCg==';
    'VerifierAdvanced'='UXVlcnkK';
    'vmware-esx'='PHNvYXA6RW52ZWxvcGUgeG1sbnM6eHNkPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTFNjaGVtYSIgeG1sbnM6eHNpPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTFNjaGVtYS1pbnN0YW5jZSIgeG1sbnM6c29hcD0iaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvc29hcC9lbnZlbG9wZS8iPjxzb2FwOkhlYWRlcj48b3BlcmF0aW9uSUQ+MDAwMDAwMDEtMDAwMDAwMDE8L29wZXJhdGlvbklEPjwvc29hcDpIZWFkZXI+PHNvYXA6Qm9keT48UmV0cmlldmVTZXJ2aWNlQ29udGVudCB4bWxucz0idXJuOmludGVybmFsdmltMjUiPjxfdGhpcyB4c2k6dHlwZT0iTWFuYWdlZE9iamVjdFJlZmVyZW5jZSIgdHlwZT0iU2VydmljZUluc3RhbmNlIj5TZXJ2aWNlSW5zdGFuY2U8L190aGlzPjwvUmV0cmlldmVTZXJ2aWNlQ29udGVudD48L3NvYXA6Qm9keT48L3NvYXA6RW52ZWxvcGU+';
    'vp3'='dnAz';
    'WMSRequest'='AQAA/c76C7CgAAAATU1TFAAAAAAAAAAAAAAAAAAAABIAAAABAAMA8PDw8AsABAAcAAMATgBTAFAAbABhAHkAZQByAC8AOQAuAAAuAAAuAgA5ADgAADsAIAB7AAAAAABBAEEAAAAtAABBAAAALQAAAGEAAC0AQQBBAABBAC0AAAAAAEEAAEEAQQAAQQBBAAB9AAAA4G3fXw==';
    'WWWOFFLEctrlstat'='V1dXT0ZGTEUgU1RBVFVTDQo=';
    'X11Probe'='bAALAAAAAAAAAAAA';
    'xmlsysd'='aW5pdApvZmYgYWxsCm9uIGlkZW50aXR5IHZlcnNpb24Kc2VuZApxdWl0Cg==';
    'ZendJavaBridge'='AAAAHwAAAAAAAAAMR2V0Q2xhc3NOYW1lAAAAAgQAAAAAAQA=';
}

function checkResponses($ipaddr, $port) {
    $responsesRecvd = new-object System.Collections.ArrayList;
    foreach ($key in $msgs.Keys) {
        # configure the connection
        $conn = new-object system.net.sockets.tcpclient($ipaddr,$port);
        $conn.NoDelay = $true;
        $conn.ReceiveBufferSize = 1024;
        $conn.SendBufferSize = 8;
        $stream = $conn.getstream();
        $stream.ReadTimeout = $timeoutInMS;
        $buff = new-object system.byte[] 2024;
        # toss the header aside (if any) before checking for a response
        try {
            $stream.read($buff, 0, $buff.Length);
        } catch {
        }
        # convert the b64 string into the bytes to be sent and fire it off to the remote host
        $msg = [System.Convert]::FromBase64String($msgs.Item($key))
        Write-Host ('{0}:{1} - ({2} test) sending message...' -f $ipaddr,$port,$key)
        $stream.Write($msg,0,$msg.Length);
        $output = '';
        # try to read the response.
        try {
            $bytesread = $stream.read($buff, 0, $buff.Length);
        } catch {
            $bytesread = -1;
        }
        while ($bytesread -gt 0) {
            $output += $enc.getstring($buff, 0, $bytesread);
            try {
                $bytesread = $stream.read($buff, 0, $buff.Length);
            } catch {
                $bytesread = -1;
            }
        }
        if ([string]::IsNullOrEmpty($output)) {
            Write-Host ('{0}:{1} - ({2} test) received no response' -f $ipaddr, $port, $key)
        } else {
            Write-Host (
                '{0}:{1} - ({2} test) received response: {3}' -f 
                $ipaddr, 
                $port, 
                $key, 
                $output
            );
            $responsesRecvd.Add($key);
        }
        $conn.close();
    }
    return $responsesRecvd.ToArray();
}

function checkTCPServices($ipaddr, $port) {
    $conn = new-object system.net.sockets.tcpclient($ipaddr,$port);
    $stream = $conn.getstream();
    $writeable = $stream.CanWrite;
    # special case for the banner - we want to render it in hex and attempt ASCII
    start-sleep -m $timeoutInMS;
    $buff = new-object system.byte[] 1024;
    $bannerstr = '';
    $bannerhex = '';
    while ($stream.DataAvailable) {
        $read = $stream.read($buff,0,1024);
        $bannerstr += $enc.getstring($buff, 0, $read);
        $bannerhex += [System.BitConverter]::ToString($buff)
        start-sleep -m 50;
    }
    $bannerhex = $bannerhex.Replace('-00-00', '');
    if ([string]::IsNullOrEmpty($bannerhex)) {
        Write-Host ('no TCP service banner found on port {0}' -f $port)
    } else {
        Write-Host ('BANNER (HEX)  : {0}' -f  $bannerhex);
        Write-Host ('BANNER (ASCII): {0}' -f  $bannerstr);
    }
    $conn.close();
    # now check to see if there are any identifiable services that seem to be listening on the port
    if ($writeable) {
        $servicesFound = (checkResponses $ipaddr $port);
    } else {
        Write-Host ('{0}:{1} - connection not writeable' -f $ipaddr, $port);
        $servicesFound = @();
    }
    # powershell does silly things with -join on arraylists, so I wrote my own.
    $serviceStr = '';
    for ($i=$servicesFound.Length/2;$i -lt $servicesFound.Length;$i++) {
        $serviceStr += $servicesFound[$i];
        if ($i -lt $servicesFound.Length -1) {
            $serviceStr += ', '
        }
    }
    if ([string]::IsNullOrEmpty($serviceStr)) {
        $out = '{0}:{1} - is open but received no service responses' -f $ipaddr, $port;
    } else {
        $out = '{0}:{1} - received service responses for {2}' -f $ipaddr, $port, $serviceStr;
    }
    $out;
}

function TCPPortScan([string]$ipaddr, [int]$start=1, [int]$end=1024) {
    # this is going to be painfully slow; but it'll work in the absence of better tools (eg. nmap)
    $openports = new-object System.Collections.ArrayList
    Write-Host (
        'Checking for open TCP ports on {0}. Scanning {1} ports starting at {2}' -f 
        $ipaddr, 
        ($end-$start+1), 
        $start
    );
    for ($i=$start; $i -le $end; $i++) {
        $open = IsTCPListening $ipaddr $i;
        if ($open) {
            Write-Host ('TCP port {0} is open on {1} - checking for services...' -f $i, $ipaddr);
            $srvcs = checkTCPServices $ipaddr $i;
            Write-Host $srvcs;
            $openports.Add($srvcs);
        }
    }
    $openports;
}

function IsTCPListening([string]$ipaddr, [int]$port) {
    try {
        $sock= New-Object System.Net.Sockets.TCpClient($ipaddr, $port);
        $open = $sock.Connected;
        $sock.Close();
    } catch {
        $open = $false
    }
    $open;
}

TCPPortScan $ipaddr $startPort $endPort