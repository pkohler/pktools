# app key, secret and callback can be acquired/generated from the dropbox app console:
# https://www.dropbox.com/developers/apps
# response_type should normally be "token"

# access_token, token_type and uid can be retrieved from the callback url once a user has authorized
# your app

# dropbox API documentation is available at https://www.dropbox.com/developers/documentation/http/documentation

param (
    [string]$app_key,                       # your application's app key
    [string]$access_token,                  # access_token for the dropbox account (obtained from the callback)
    [string]$callback = "http://localhost", # your application's callback uri
    [string]$response_type = "token",       # must be `code` or `token`
    [string]$token_type = "bearer",
    # If $dropbox_folder is left blank, it will upload to /Apps/yourAppName/filename.ext
    [string]$dropbox_folder = "",           
    [string]$filepath                       # what local file to upload
)

$ErrorActionPreference = "Stop"
Add-Type -AssemblyName System.Web



function httpAddUrlParams([string]$url, [hashtable]$urlparams) {
    $pcount = $urlparams.Count
    if ($pcount -gt 0){
        $url += "?"
        $cur = 1
        foreach ($p in $urlparams.Keys) {
            $epk = $p
            $epv = $urlparams[$p]
            $url += "$epk=$epv"
            if ($cur -lt $pcount) {
                $url += "&"
            }
            $cur++
        }
    }
    $url
}

function httpPrintResponse($resp) {
    $stream = $resp.GetResponseStream()
    $reader = New-Object System.IO.StreamReader($stream)
    $reader.ReadToEnd()
}

function urlEncode([string]$data) {
    $encoded = [System.Web.HttpUtility]::UrlEncode($data)
    $encoded
}

function htmlEscape([string]$data) {
    $encoded = [System.Web.HttpUtility]::HtmlEncode($data)
    $encoded
}

function drpbGetFullUrl([string]$apimethod) {
    # returns the URL with the correct parameters entered for you
    $o = $apiUrls[$apimethod]
    $url = httpAddUrlParams $o["url"]
    if ($o.ContainsKey("urlparams")) {
        $url = httpAddUrlParams $url $o["urlparams"]
    }
    $url
}

function drbpReq([string]$apimethod) {
    $o = $apiUrls[$apimethod]
    $url = drpbGetFullUrl $apimethod
    $req = [System.Net.WebRequest]::Create($url)
    $req.Method = $o["method"]
    if ($o.ContainsKey("headers")) {
        foreach ($h in $o["headers"].Keys) {
            $req.Headers[$h] = $o["headers"][$h]
        }
    }
    if ($o.ContainsKey("data")) {
        $req.ContentType = $o["contenttype"]
        $data = $o["data"]
        if ($o["contenttype"] -like "text*") {
            $data = [System.Text.Encoding]::UTF8.GetBytes($ata)
        }
        $stream = $req.GetRequestStream()
        $stream.Write($data, 0, $data.Length)
        $stream.Close()
    } else {
        # probably nothing to do
    }
    $resp = $req.GetResponse()
    $resp
}

$drpb_url = "https://www.dropbox.com"
$drpb_api_url = "https://content.dropboxapi.com"
$callback = urlEncode $callback
$apiUrls = @{
    "authorize" = @{
        "url" = "$drpb_url/oauth2/authorize";
        "method" = "GET";
        "urlparams" = @{
            "response_type"=$response_type;
            "client_id"=$app_key;
            "redirect_uri"=$callback;
        }
    };
    "upload" = @{
        # according to the API docs, /2/files/upload should only be used for files <150mb
        "url" = "$drpb_api_url/2/files/upload";
        "method" = "POST";
        "contenttype" = "application/octet-stream";
        "headers" = @{
            "Authorization" = "$token_type $access_token";
            "Dropbox-API-Arg" = "";
        }
    }
}

$apiUrls["upload"]["headers"]["dropbox-api-arg"] = ConvertTo-Json @{
    "path" = "$dropbox_folder/$(Split-Path $filepath -Leaf)";
    "mode" = "add";
    "autorename" = $true;
    "mute" = $false;
} -Compress
$bytes = Get-Content $filepath -Encoding Byte
$apiUrls["upload"]["data"] = $bytes
$r = drbpReq "upload"
httpPrintResponse $r