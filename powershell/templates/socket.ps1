$ErrorActionPreference = 'Stop';

function tcpping([string]$ipaddr, [int]$port) {
    try {
        $sock= New-Object System.Net.Sockets.TCpClient($ipaddr, $port);
        $sock.SendTimeout = 1000;
        $sock.ReceiveTimeout = 1000;
        $open = $sock.Connected;
        $sock.Close();
    } catch {
        $open = $false
    }
    $open;
}

function GetBanner([string]$ipaddr, [int]$port) {
    $sock= New-Object System.Net.Sockets.TCpClient($ipaddr, $port);
    $stream = $sock.GetStream();
    $buff = New-Object System.Byte[] 1024;
    $enc = New-Object System.Text.UnicodeEncoding;
    Start-Sleep -m 6000;
    $output = '';
    while ($stream.DataAvailable) {
        $read = $stream.read($buff,0,1024);
        $output += $enc.GetString($buff,0,$read);
        Start-Sleep -m 500;
    }
    $sock.Close();
    $output;
}

function TCPPortScan([string]$ipaddr, [int]$start=1, [int]$end=1024) {
    # this is going to be painfully slow; but it'll work in the absence of better tools (eg. nmap)
    $out = 'Checking for open TCP ports on {0}. Scanning {1} ports starting at {2}' -f $ipaddr, ($end-$start+1), $start;
    Write-Host $out;
    for ($i=$start; $i -lt $end; $i++) {
        $open = IsTCPListening $ipaddr $i;
        if ($open) {
            $out = 'TCP port {0} is open on {1} - checking banner...' -f $i, $ipaddr;
            Write-Host $out;
            $out = GetBanner $ipaddr $i;
            if ([string]::IsNullOrEmpty($out)) {
                Write-Host 'No TCP banner received';
            } else {
                Write-Host $out;
            }
        }
    }
}

TCPPortScan '127.0.0.1'
#one-liner mode
$ipaddr='127.0.0.1'; $port=80; $ErrorActionPreference = 'Stop';try{$sock=New-Object System.Net.Sockets.TCpClient($ipaddr, $port);$open=$sock.Connected; $sock.Close();} catch {$open = $false} $open;