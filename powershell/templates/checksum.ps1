$md5 = New-Object -TypeName System.Security.Cryptography.MD5CryptoServiceProvider;
$sha1 = New-Object -TypeName System.Security.Cryptography.SHA1CryptoServiceProvider;
$sha256 = New-Object -TypeName System.Security.Cryptography.SHA256CryptoServiceProvider;
$sha512 = New-Object -TypeName System.Security.Cryptography.SHA512CryptoServiceProvider;

function Get-Hash([string]$filepath, $cryptoProv) {
    $hash = $cryptoProv.ComputeHash([System.IO.File]::ReadAllBytes($filepath));
    [System.BitConverter]::ToString($hash).Replace("-","");
}

function Get-Hashes([string]$filepath) {
    Write-Host ('MD5: {0}' -f (Get-Hash $filepath $md5));
    Write-Host ('sha1: {0}' -f (Get-Hash $filepath $sha1));
    <# Write-Host ('sha256: {0}' -f (Get-Hash $filepath $sha256)); #>
    <# Write-Host ('sha512: {0}' -f (Get-Hash $filepath $sha512)); #>
}

Get-Hashes "C:\Temp\MWDiag.gpg"