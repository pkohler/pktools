function packb64([string]$sourcefile, [string]$destfile="") {
    # Encode a file's binary data into a base-64 string and save it to a new file
    # or return its b64 value to be manipulated further.
    $bytes = Get-Content $sourcefile -Encoding Byte
    $encoded = [System.Convert]::ToBase64String($bytes)
    $wroteFile = $false
    if (![string]::IsNullOrEmpty($destfile)) {
        [io.file]::WriteAllText($destfile, $encoded)
        $wroteFile = $true
    }
    Write-Host "encoded $sourcefile in base64..."
    if ($wroteFile) {
        Write-Host "wrote encoded data to $destfile"
    } else {
        # echo/return encoded data
        $encoded
    }
}

function unpackb64([string]$b64, [string]$filename) {
    # Decode a base-64 string into binary data and save it to a file 
    # Overwrites file if it already exists
    $bytes = [System.Convert]::FromBase64String($b64)
    $file = [io.file]::WriteAllBytes($filename, $bytes)
}