# uint32 values for different registry hives
# https://imgur.com/hWmio4o.gif
# (these values are actually defined in winreg.h)
[uint32]$hkclassesroot = 2147483648;   # 0x80000000
[uint32]$hkcu = 2147483649;            # 0x80000001
[uint32]$hklm = 2147483650;            # 0x80000002
[uint32]$hku = 2147483651;             # 0x80000003
[uint32]$hkcurrentconfig = 2147483653; # 0x80000005

# create a stdRegProv CIM object and set key and valname variables
$reg = [WMIClass]'root\default:StdRegProv';
$key = 'SOFTWARE\Level Platforms\Managed Workplace\Onsite Manager\Install';
$valname = 'ConnectionString';

# now call any stdRegProv methods 
# https://msdn.microsoft.com/en-us/library/aa393664%28v=vs.85%29.aspx
$val = $reg.GetStringValue($hklm, $key, $valname).sValue; # regSZ
$val = $reg.GetDwordValue($hklm, $key, $valname).dValue;  # regDW
# etc.