# ignore SSL errors
[System.Net.ServicePointManager]::ServerCertificateValidationCallback = {$true};

function httpGet([string]$url, [hashtable]$headers=@{}) {
    $req = [System.Net.WebRequest]::Create($url);
    foreach ($h in $headers.Keys) {
        $req.Headers[$h.ToString()] = $headers[$h].ToString();
    }
    $resp = $req.GetResponse();
    $resp;
}

function httpPost([string]$url, [string]$data, [string]$contenttype, [hashtable]$headers=@{}) {
    $req = [System.Net.WebRequest]::Create($url);
    $req.Method = "POST";
    $req.ContentType=$contenttype;
    $bdata = [System.Text.Encoding]::UTF8.GetBytes($data);
    $req.ContentLength = $bdata.Length;
    foreach ($h in $headers.Keys) {
        $req.Headers[$h.ToString()] = $headers[$h].ToString();
    }
    $rstream = $req.GetRequestStream();
    $rstream.Write($bdata, 0, $bdata.Length);
    $rstream.Close();
    $resp = $req.GetResponse();
    $resp;
}

function httpPrint($resp) {
    $stream = $resp.GetResponseStream();
    $reader = New-Object System.IO.StreamReader($stream);
    $reader.ReadToEnd();
}

function getPage([string]$url) {
    # perform an HTTP GET to $url and then show the response as text.
    # if you need to supply headers or print from a POST, use httpGet or httpPost.
    $resp = httpGet $url;
    httpPrint $resp;
}

function download([string]$url, [string]$destination) {
    # this assumes a publicly available URL that doesn't require headers or POST data.
    # if the URL needs those, use dlFromResponse instead
    $resp = httpGet $url;
    dlFromResponse $resp $destination
}

function dlFromResponse($resp, [string]$destination) {
    # $resp should be an WebResponse eg. from httpGet or httpPost
    $hstream = $resp.GetResponseStream();
    $fstream = [io.file]::Create($destination);
    $hstream.CopyTo($fstream);
    $fstream.Close();
}

# one-liner mode for simple GET requests with no headers etc.
$url='http://localhost:30861/reauth';$s=[System.Net.WebRequest]::Create($url).GetResponse();$s;$r=New-Object System.IO.StreamReader($s.GetResponseStream());$r.ReadToEnd();

# download using bitstransfer instead of [System.Net]
$url='https://sourceurl.com/file.tar.gz';$destfile='C:\foo\bar\file.tar.gz';import-module bitstransfer;start-bitstransfer -source $url -destination $destfile;