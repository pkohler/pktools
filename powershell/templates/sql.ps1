function sqlQuery([string]$connectionString, [string]$query, [switch]$isCE=$false) {
    # query an MSSQL database
    if (!$isCE) {
        # connect to a normal SQL Server/SQL Server Express database
        $connection = New-Object System.Data.SqlClient.SqlConnection
    } else { 
        # connect to a SQLCE database.
        # make sure you've added the dll into powershell, eg:
        # Add-Type -Path "C:\Path\To\System.Data.SqlServerCe.dll"
        $connection = New-Object System.Data.SqlServerCe.SqlCeConnection
    }
    $connection.ConnectionString = $connectionString
    $connection.Open()
    $command = $connection.CreateCommand()
    $command.CommandText = $query
    $result = $command.ExecuteReader()
    # load query results into a table
    $table = New-Object System.Data.DataTable
    $table.Load($result)
    $connection.Close()
    $table  # display/return query results
}

function sqlExecute([string]$connectionString, [string]$cmd, [switch]$isCE=$false) {
    # execute a non-query command on an MSSQL database
    if (!$isCE) { 
        # connect to a normal SQL Server/SQL Server Express database
        $connection = New-Object System.Data.SqlClient.SqlConnection
    } else { 
        # connect to a SQLCE database.
        # make sure you've added the dll into powershell, eg:
        # Add-Type -Path "C:\Path\To\System.Data.SqlServerCe.dll"
        $connection = New-Object System.Data.SqlServerCe.SqlCeConnection
    }
    $connection.ConnectionString = $connectionString
    $connection.Open()
    $command = $connection.CreateCommand()
    $command.CommandText = $cmd
    $result = $command.ExecuteNonQuery()
    $connection.Close()
    $result  # display/return num of rows affected
}