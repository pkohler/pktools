import requests
import sys
import os
import time
import re
import socket
from termcolor import colored


packet_size = 32768  # 32 KiB
mebibyte = 1048576
timeout = 2.0
reFileName = re.compile(r'.*?\.[a-zA-Z0-9]+')

def __init__():
    pass


def show_prog(size, current, mb):
    # print a color-coded progress % to the console.
    progress = float(current / size * 100)
    if progress > 100:
        progress = 100
    colors = ['red', 'yellow', 'blue', 'green', 'green']
    progcolor = colors[int((progress / 100) * 4)]
    sys.stdout.write(colored('%0.2f percent of %f MiB\r' % (progress, mb), progcolor))


def dl_iter(url, save_loc, session):
    r = session.get(url, stream=True, timeout=timeout)
    size = float(r.headers['Content-Length'])
    mb = size / mebibyte
    # print('saving %s - %0.2f MiB' % (save_loc, (size / mebibyte)))
    try:
        with open(save_loc, 'wb') as f:
            current_dl = 0
            for chunk in r.iter_content(packet_size):
                current_dl += packet_size
                f.write(chunk)
                show_prog(size, current_dl, mb)
    except (
        requests.exceptions.ConnectionError,
        requests.exceptions.Timeout,
        requests.exceptions.ReadTimeout,
        socket.timeout,
        TypeError,
        ):
        # this usually means the connection was interrupted
        # so wait 1 second and just try again
        time.sleep(1)
        dl(url, save_loc=save_loc, session=session)


def dl_full(url, save_loc, session):
    try:
        r = session.get(url, timeout=timeout)
        with open(save_loc, 'wb') as f:
            f.write(r.content)
        sys.stdout.write(colored('100.00 percent complete\r', 'green'))
    except (requests.exceptions.ConnectionError, requests.exceptions.Timeout):
        time.sleep(1)
        dl_full(url, save_loc=save_loc, session=session)


def dl(url, save_loc=None, session=requests.Session(), referer=None):
    # do the actual downloading
    if not save_loc:
        save_loc = os.getcwd()
    if referer:
        session.headers['Referer'] = referer
    try:
        dl_iter(url, save_loc, session)
    except KeyError:
        dl_full(url, save_loc, session)


def save_url(
    url,
    savefolder=None,
    prepend='',
    filename=None,
    overwrite=False,
    session=requests.Session(),
    show_exits=False,
    lowercase=False
):
    # utility function for ensuring dir exists, what to do with existing files, etc.
    # call this rather than using `_dl` dircetly.
    if not savefolder:
        savefolder = os.path.getcwd()
    if lowercase:
        savefolder = savefolder.lower()
    if not filename:
        # use the existing one from the url. drop any session or url specific junk
        filename = reFileName.findall(url.split('/')[-1])[0]
    topdir, fldr = os.path.split(savefolder)
    if len(fldr) > 254:
        savefolder = os.path.join(topdir, fldr[:200])
    if not os.path.exists(savefolder):
        os.makedirs(savefolder)
    save_loc = os.path.join(savefolder, prepend + filename)
    if overwrite and os.path.exists(save_loc):
        os.remove(save_loc)
    if not os.path.isfile(save_loc):
        print('saving {}...'.format(save_loc))
        dl(url, save_loc, session=session)
        # print('\n{} saved'.format(filename))
        return True
    else:
        if show_exits:
            print('\n{} already exists'.format(filename))
        return False
