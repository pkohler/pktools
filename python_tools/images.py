import os
import shutil
import subprocess
from PIL import Image
from filenames import *
from folders import *


def __init__():
    pass


def combine_webtoon(folderpath=None):
    # combine a folder of images into one vertically stacked .png
    if not folderpath:
        folderpath = os.getcwd()
    saveloc, filename = split_folder(folderpath)
    images = [Image.open(os.path.join(folderpath, i)) for i in
              sorted(os.listdir(folderpath),
                     key=lambda x: os.path.getctime(os.path.join(folderpath, x)))]
    combined_height = sum([i.size[1] - 1 for i in images])
    max_width = max([i.size[0] for i in images])
    combo = Image.new('RGBA', (max_width, combined_height))
    current_y = 0
    for img in images:
        combo.paste(img, box=(0, current_y-1))
        current_y += img.size[1]-1
    combo.save(
        '{}.png'.format(os.path.join(saveloc, filename.zfill(3)), 'png'))
    print("Saved {}.png".format(os.path.join(saveloc, filename.zfill(3))))
    try:
        shutil.rmtree(folderpath)
    except Exception as e:
        print(e)
    return


def convert_to_png(filepath):
    # convert all images to .png
    name, ext = os.path.splitext(filepath)
    if ext.lower() in ['bmp', 'jpg', 'jpeg']:
        Image.open(filepath).save('{}.png'.format(name), 'png')
        print(name, 'converted to .png')
    else:
        pass


def folder_to_png(folderpath=None):
    contents = get_files(folderpath)
    for f in contents:
        try:
            convert_to_png(os.path.join(folderpath, f))
        except Exception as e:
            print(os.path.join(folderpath, f), e)


def resize_img(new_x, new_y, src, dst=None):
    if not dst:
        dst = src
    i = Image.open(src)
    i = i.resize((new_x, new_y))
    i.save(dst)
    print('resized {} to {}x{} and saved to {}'.format(src, new_x, new_y, dst))


def resize_img_folder(newx, newy, path=None):
    if not path:
        path = os.getcwd()
    files = get_files(path)
    for f in files:
        resize_img(os.path.join(path, f), os.path.join(
            path, 'resized', f), newx, newy)


def make_timelapse(folder=None, framerate=1):
    if folder:
        outfile = os.path.join(folder, split_folder(folder)[1])
    else:
        folder = os.getcwd()
        outfile = os.path.join(folder, split_folder(folder)[1])
        folder = ''
    print('generating video file...')
    cmd = [
        'ffmpeg',
        '-pattern_type',
        'glob',
        '-i',
        os.path.join(folder, '*.png'),
        '-r',
        str(framerate),
        '-c:v',
        'libx264',
        '{}.mp4'.format(outfile),
        '-y'
    ]
    print(' '.join(cmd))
    subprocess.call(cmd)
