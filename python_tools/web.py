import requests
import json

def f_to_c(temp):
    #F = (C x 9/5) + 32
    #C = F/(9/5) - 15/(9/5)
    return temp/(9/5) - 15/(9/5)


def forecast(coords=('45.42,-75.69')):
    s = requests.Session()
    r = s.get('http://forecast.io/#/f/45.4217,-75.6912')
    s.headers = {}
    s.headers['Host'] = 'forecast.io'
    s.headers['User-Agent'] = 'Mozilla/5.0 (X11; Linux x86_64; rv:29.0) Gecko/20100101 Firefox/29.0'
    s.headers['Referrer'] = 'http://forecast.io/'
    r = s.get('http://forecast.io/forecast?q=45.42,-75.69&satellites&si&raw').text
    print(r)
    '''
    forecast_js = json.loads(r)
    today = forecast_js['daily']['data'][0]
    mintemp = today['temperatureMin']
    maxtemp = today['temperatureMax']
    print(today['summary'], 'High:', maxtemp, 'Low:', mintemp)
    '''

if __name__ == '__main__':
    forecast()