import os
import shutil
import zipfile
import re
from filenames import *


page0 = re.compile('000+')

def __init__():
    pass


def combine_cbz(folder=None):
    # combine each chapter of a comic into a .cbz file in the parent directory
    if folder:
        os.chdir(folder.replace('file://', '').strip())
    cwd = os.getcwd()
    comicname = split_folder(cwd)[1]
    for folderpath in [i for i in os.listdir() if os.path.isdir(i)]:
        saveloc, f = os.path.split(folderpath)
        f = '{}-{}.cbz'.format(comicname, f.zfill(5))
        filename = os.path.join(cwd, saveloc, f)
        os.chdir(os.path.join(cwd, folderpath))
        zargs = {
            "file": filename,
            "mode": "w",
            "compression": zipfile.ZIP_STORED,  # compression won't do much to a compressed image
            }
        with zipfile.ZipFile(**zargs) as z:
            for f in os.listdir():
                name, ext = os.path.splitext(f)
                if 'jpg' in ext:
                    os.rename(f, '{}.jpg'.format(name))
                elif 'png' in ext:
                    os.rename(f, '{}.png'.format(name))
            for f in os.listdir():
                z.write(f)
        print('{} written to disk'.format((filename)))
    return


def rename_contents(folder=None, digits=9, by_ctime=True):
    # renames contents of a folder to a n-digit number, ordered by creation time
    if not folder:
        folder = os.getcwd()
    if by_ctime:
        contents = sorted(get_files(folder), key=lambda x: os.path.getctime(x))
    else:
        contents = sorted(get_files(folder))
    contents = [i for i in contents if 'url.txt' not in i]
    for index, f in enumerate(contents):
        filetype = os.path.splitext(f)[1]
        if not filetype:
            filetype = ''
        os.rename(f, os.path.join(folder, str(index).zfill(digits) + filetype))
    return


def flatten_folder(folder=None):
    # move all contents of a folder to its top directory
    if not folder:
        folder = os.getcwd()
    subfolders = get_folders(folder)
    for s in subfolders:
        subdirs = get_folders(s)
        for d in subdirs:
            flatten_folder(d)
            shutil.rmtree(d)
        subfiles = get_files(s)
        for i in subfiles:
            newname = '{}_{}'.format(os.path.split(s)[1], os.path.split(i)[1])
            shutil.move(i, os.path.join(folder, newname))
    return


def chapter_exists(chapterpath, webtoon=False):
    # has a chapter already been downloaded?
    if webtoon and os.path.exists('{}.png'.format(chapterpath).lower()):
        return True
    elif not webtoon and os.path.exists(chapterpath.lower()):
        listing = os.listdir(chapterpath.lower())
        if listing != []:
            if page0.match(listing[0]):
                # chapter was completely downloaded and files inside it were renamed
                return True
    return False


def tv_season(folder=None):
    if not folder:
        folder = os.getcwd()
    episode = re.compile(r'[sS]?\d?\d\.?[eE]?\d{1,2}')
    excep = re.compile(r'((720)|(264)|([12]\d{3}))')
    for e in os.listdir(folder):
        found = episode.findall(e)
        print([i for i in found if not excep.match(i)])
