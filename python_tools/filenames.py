import os
import re


def __init__():
    pass


def split_folder(path):
    # take a path string and split it properly
    split = os.path.split(path)
    if split[1] == '':
        split = os.path.split(split[0])
    return split


def get_files(folder=None):
    if not folder:
        folder = os.getcwd()
    files = sorted([
        os.path.join(folder, f)
        for f in os.listdir(folder)
        if os.path.isfile(os.path.join(folder, f))
    ])
    return files


def get_folders(folder=None):
    if not folder:
        folder = os.getcwd()
    fldrs = sorted([
        os.path.join(folder, f)
        for f in os.listdir(folder)
        if os.path.isdir(os.path.join(folder, f))
    ])
    return fldrs


def standardize_braces(string):
    # change {},(), and <> into [], remove comiket stamp
    sub = re.sub(r'\{|\(|\<', '[', string)   # open braces to [
    sub = re.sub(r'\}|\)|\>', ']', sub)      # close braces to ]
    sub = re.sub(r'\[(C|c)\d+\]', '', sub)  # comiket stamps to nothing
    sub = re.sub(r'[\\/]', '.', sub)         # (back)slashes to .s
    return sub.strip()
